import { loadFixture } from "@nomicfoundation/hardhat-toolbox/network-helpers";
import { expect } from "chai";
import { ethers } from "hardhat";

import { BankAccount } from "../typechain-types/BankAccount";

import type { HardhatEthersSigner } from "@nomicfoundation/hardhat-ethers/signers";

describe("BankAccount", function () {
    // We define a fixture to reuse the same setup in every test.
    // We use loadFixture to run this setup once, snapshot that state,
    // and reset Hardhat Network to that snapshot in every test.
    async function deployBankAccount() {
        // Contracts are deployed using the first signer/account by default

        // There are 10 accounts provided
        const [addr0, addr1, addr2, addr3, addr4] = await ethers.getSigners();

        const BankAccount = await ethers.getContractFactory("BankAccount");
        const bankAccount = await BankAccount.deploy();

        return { bankAccount, addr0, addr1, addr2, addr3, addr4 };
    }

    async function deployBankAccountWithAccounts(
        owners = 1,
        deposit = 0,
        withdrawalAmounts: number[] = []
    ) {
        const { bankAccount, addr0, addr1, addr2, addr3, addr4 } = await loadFixture(
            deployBankAccount
        );
        const addresses: string[] = [];

        if (owners > 1) addresses.push(addr1.address);
        if (owners > 2) addresses.push(addr2.address);
        if (owners > 3) addresses.push(addr3.address);

        const signer = getSigner(bankAccount, addr0);

        await signer.createAccount(addresses);

        if (deposit > 0) await signer.deposit(0, { value: deposit.toString() });

        for (const withdrawalAmount of withdrawalAmounts)
            await signer.requestWithdrawal(0, withdrawalAmount);

        return { bankAccount, addr0, addr1, addr2, addr3, addr4 };
    }

    describe("Deployment", () => {
        it("Should deploy without error", async () => {
            await loadFixture(deployBankAccount);
        });
    });

    describe("Creating an account", () => {
        it("should allow creating a single user account", async () => {
            const { bankAccount, addr0 } = await loadFixture(deployBankAccount);

            const signer = getSigner(bankAccount, addr0);
            await signer.createAccount([]);

            const accounts = await signer.getAccounts();
            expect(accounts.length).to.equal(1);
        });

        it("should allow creating a double user account", async () => {
            const { bankAccount, addr0, addr1 } = await loadFixture(deployBankAccount);

            const signer = getSigner(bankAccount, addr0);
            await signer.createAccount([addr1.address]);

            const accounts0 = await signer.getAccounts();
            expect(accounts0.length).to.equal(1);

            const accounts1 = await getSigner(bankAccount, addr1).getAccounts();
            expect(accounts1.length).to.equal(1);
        });

        it("should allow creating a triple user account", async () => {
            const { bankAccount, addr0, addr1, addr2 } = await loadFixture(deployBankAccount);

            const signer = getSigner(bankAccount, addr0);
            await signer.createAccount([addr1.address, addr2.address]);

            const accounts0 = await signer.getAccounts();
            expect(accounts0.length).to.equal(1);

            const accounts1 = await getSigner(bankAccount, addr1).getAccounts();
            expect(accounts1.length).to.equal(1);

            const accounts2 = await getSigner(bankAccount, addr2).getAccounts();
            expect(accounts2.length).to.equal(1);
        });

        it("should allow creating a quad user account", async () => {
            const { bankAccount, addr0, addr1, addr2, addr3 } = await loadFixture(
                deployBankAccount
            );

            const signer = getSigner(bankAccount, addr0);
            await signer.createAccount([addr1.address, addr2.address, addr3.address]);

            const accounts0 = await signer.getAccounts();
            expect(accounts0.length).to.equal(1);

            const accounts1 = await getSigner(bankAccount, addr1).getAccounts();
            expect(accounts1.length).to.equal(1);

            const accounts2 = await getSigner(bankAccount, addr2).getAccounts();
            expect(accounts2.length).to.equal(1);

            const accounts3 = await getSigner(bankAccount, addr3).getAccounts();
            expect(accounts3.length).to.equal(1);
        });

        it("should not allow creating an account with duplicate owners", async () => {
            const { bankAccount, addr0 } = await loadFixture(deployBankAccount);

            const signer = getSigner(bankAccount, addr0);
            await expect(signer.createAccount([addr0.address])).to.be.reverted;
        });

        it("should not allow creating an account with 5 owners", async () => {
            const { bankAccount, addr0, addr1, addr2, addr3, addr4 } = await loadFixture(
                deployBankAccount
            );

            const signer = getSigner(bankAccount, addr0);
            await expect(
                signer.createAccount([
                    addr0.address,
                    addr1.address,
                    addr2.address,
                    addr3.address,
                    addr4.address,
                ])
            ).to.be.reverted;
        });

        it("should not allow a user to create more than 3 accounts", async () => {
            const { bankAccount, addr0 } = await loadFixture(deployBankAccount);

            for (let i = 0; i < 3; i++) await getSigner(bankAccount, addr0).createAccount([]);

            await expect(getSigner(bankAccount, addr0).createAccount([])).to.be.reverted;
        });
    });

    describe("Depositing", () => {
        it("should allow deposit from account owner", async () => {
            const { bankAccount, addr0 } = await deployBankAccountWithAccounts(1);
            const signer = getSigner(bankAccount, addr0);

            await expect(signer.deposit(0, { value: "100" })).to.changeEtherBalances(
                [bankAccount, addr0],
                [100, -100]
            );
        });

        it("should not allow deposit from non-account owner", async () => {
            const { bankAccount, addr1 } = await deployBankAccountWithAccounts(1);
            const signer = getSigner(bankAccount, addr1);

            await expect(signer.deposit(0, { value: "100" })).to.be.reverted;
        });
    });

    describe("Withdraw", () => {
        describe("Request a withdraw", () => {
            it("account owner can request withdrawal", async () => {
                const { bankAccount, addr0 } = await deployBankAccountWithAccounts(1, 100);

                const signer = getSigner(bankAccount, addr0);
                signer.requestWithdrawal(0, 100);
            });

            it("account owner cannot request withdrawal with invalid amount", async () => {
                const { bankAccount, addr0 } = await deployBankAccountWithAccounts(1, 100);

                const signer = getSigner(bankAccount, addr0);
                await expect(signer.requestWithdrawal(0, 101)).to.be.reverted;
            });

            it("non-account owner cannot request withdrawal", async () => {
                const { bankAccount, addr1 } = await deployBankAccountWithAccounts(1, 100);

                const signer = getSigner(bankAccount, addr1);
                await expect(signer.requestWithdrawal(0, 90)).to.be.reverted;
            });

            it("account owner can request multiple withdrawa;", async () => {
                const { bankAccount, addr0 } = await deployBankAccountWithAccounts(1, 100);

                const signer = getSigner(bankAccount, addr0);
                signer.requestWithdrawal(0, 90);
                signer.requestWithdrawal(0, 10);
            });
        });

        describe("Approve a withdraw", () => {
            it("should allow account onwer to approve withdrawal request", async () => {
                const { bankAccount, addr1 } = await deployBankAccountWithAccounts(2, 100, [100]);

                const signer = getSigner(bankAccount, addr1);
                await signer.approveWithdrawal(0, 0);

                expect(await signer.getApprovals(0, 0)).to.equal(1);
            });

            it("should not allow non-account onwer to approve withdrawal request", async () => {
                const { bankAccount, addr2 } = await deployBankAccountWithAccounts(2, 100, [100]);

                const signer = getSigner(bankAccount, addr2);

                await expect(signer.approveWithdrawal(0, 0)).to.be.reverted;
            });

            it("should not allow account onwer to approve withdrawal request multiple times", async () => {
                const { bankAccount, addr1 } = await deployBankAccountWithAccounts(2, 100, [100]);

                const signer = getSigner(bankAccount, addr1);
                await signer.approveWithdrawal(0, 0);

                await expect(signer.approveWithdrawal(0, 0)).to.be.reverted;
            });

            it("should not allow request creator to approve withdrawal request", async () => {
                const { bankAccount, addr0 } = await deployBankAccountWithAccounts(2, 100, [100]);

                const signer = getSigner(bankAccount, addr0);
                await expect(signer.approveWithdrawal(0, 0)).to.be.reverted;
            });
        });

        describe("Make withdraw", () => {
            it("should allow request creator to withdraw an approved request", async () => {
                const { bankAccount, addr0, addr1 } = await deployBankAccountWithAccounts(2, 100, [
                    100,
                ]);

                await getSigner(bankAccount, addr1).approveWithdrawal(0, 0);
                await getSigner(bankAccount, addr0).withdraw(0, 0);
            });

            it("should allow request creator to withdraw an approved request", async () => {
                const { bankAccount, addr0, addr1 } = await deployBankAccountWithAccounts(2, 100, [
                    100,
                ]);

                await getSigner(bankAccount, addr1).approveWithdrawal(0, 0);
                await expect(getSigner(bankAccount, addr0).withdraw(0, 0)).to.changeEtherBalances(
                    [bankAccount, addr0],
                    [-100, 100]
                );
            });

            it("should not allow request creator to withdraw an approved request twice", async () => {
                const { bankAccount, addr0, addr1 } = await deployBankAccountWithAccounts(2, 200, [
                    100,
                ]);

                await getSigner(bankAccount, addr1).approveWithdrawal(0, 0);
                await expect(getSigner(bankAccount, addr0).withdraw(0, 0)).to.changeEtherBalances(
                    [bankAccount, addr0],
                    [-100, 100]
                );
                await expect(getSigner(bankAccount, addr0).withdraw(0, 0)).to.be.reverted;
            });

            it("should not allow non request creator to withdraw an approved request", async () => {
                const { bankAccount, addr1 } = await deployBankAccountWithAccounts(2, 200, [100]);

                await getSigner(bankAccount, addr1).approveWithdrawal(0, 0);
                await expect(getSigner(bankAccount, addr1).withdraw(0, 0)).to.be.reverted;
            });

            it("should not allow request creator to withdraw a non-approved request", async () => {
                const { bankAccount, addr0 } = await deployBankAccountWithAccounts(2, 200, [100]);

                await expect(getSigner(bankAccount, addr0).withdraw(0, 0)).to.be.reverted;
            });
        });
    });

    function getSigner(bankAccount: BankAccount, address: HardhatEthersSigner): BankAccount {
        return bankAccount.connect(address) as unknown as BankAccount;
    }
});
