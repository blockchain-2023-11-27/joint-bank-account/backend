# Join Bank Account (Hardhat)

Run the following tasks:

```shell
npx hardhat help
npx hardhat test
REPORT_GAS=true npx hardhat test
npx hardhat node
npx hardhat run --network localhost ./scripts/deploy.ts
```
