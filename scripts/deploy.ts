import { ethers } from "hardhat";
import fs from "fs/promises";
import { BankAccount } from "../typechain-types/BankAccount";
import type { HardhatEthersSigner } from "@nomicfoundation/hardhat-ethers/signers";
import { BaseContract } from "ethers/src.ts/contract/contract";

async function main() {
    const BankAccount = await ethers.getContractFactory("BankAccount");
    const bankAccount = await BankAccount.deploy();

    await bankAccount.waitForDeployment();
    await writeDeploymentInfo(bankAccount as any as BaseContract);

    console.log(`BankAccount deployed to ${bankAccount.target} ...`);
}

async function writeDeploymentInfo(contract: BaseContract) {
    const data = {
        contract: {
            address: contract.target,
            signerAddress: (contract.runner as HardhatEthersSigner | null)?.address,
            abi: contract.interface.format(),
        },
    };

    const content = JSON.stringify(data, null, 2);
    await fs.writeFile("deployment.json", content, "utf-8");
}
// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
    console.error(error);
    process.exitCode = 1;
});
